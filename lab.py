import os
import numpy as np

path = 'C:\\Users\\user\\Desktop\\train'
d = {}
alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
vec = []

for filename in os.listdir(path):
    with open(path + '\\' + filename, 'r', encoding='utf-8') as f:
        data = f.read()
    for i in alphabet:
        cnt = data[:2*len(data)//3].lower().count(i)
        vec.append(cnt)
        num_vect = np.array(vec, dtype=float)
        num_vect /= np.linalg.norm(num_vect, ord = 2)
    d[filename[:2]] = num_vect

print(d)